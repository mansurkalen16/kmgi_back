<?php

namespace App\Http\Controllers;

use App\Models\Refs\Field;
use App\Models\Refs\Horizon;
use App\Models\Refs\WellStatus;
use App\Models\Refs\WellType;
use Illuminate\Http\Request;

class RefsController extends Controller
{
    /**
     * Display a listing of the fields.
     *
     * @return \Illuminate\Http\Response
     */
    public function fields()
    {
        $fields = Field::all();

        return response()->json([
            "fields" => $fields
        ]);
    }
    /**
     * Display a listing of the statuses of well.
     *
     * @return \Illuminate\Http\Response
     */
    public function statuses()
    {
        $statuses = WellStatus::all();

        return response()->json([
            "statuses" => $statuses
        ]);
    }
    /**
     * Display a listing of the types of well.
     *
     * @return \Illuminate\Http\Response
     */
    public function types()
    {
        $types = WellType::all();

        return response()->json([
            "types" => $types
        ]);
    }
    /**
     * Display a listing of the horizons.
     *
     * @return \Illuminate\Http\Response
     */
    public function horizons()
    {
        $horizons = Horizon::all();

        return response()->json([
            "horizons" => $horizons
        ]);
    }
}
