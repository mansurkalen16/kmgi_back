<?php

namespace App\Http\Controllers;

use App\Http\Requests\WellPostRequest;
use App\Models\Well;
use Illuminate\Http\Request;

class WellsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wells = Well::
            with("type")->
            with("status")->
            with("horizon")->
            with("field")->
            get();

        return response()->json([
            "success" => true,
            "wells" => $wells
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WellPostRequest $request)
    {

        if(Well::create($request->all())){
            return response()->json([
                "success" => true
            ]);
        }

        return response()->json([
            "success" => false
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Well  $well
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $well = Well::find($id);

        if($well){
            return response()->json([
                "success" => true,
                "well" => $well
            ]);
        }

        return response()->json([
            "success" =>false
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Well  $well
     * @return \Illuminate\Http\Response
     */
    public function update(WellPostRequest $request, $id)
    {

        $well = Well::find($id);
        if($well &&  $well->update($request->all())){
            return response()->json([
                "success" => true
            ]);
        }

        return response()->json([
            "success" => false
        ]);
    }

    /**
     * Update column save the specified resource in storage.
     *
     * @param  \App\Models\Well  $well
     * @return \Illuminate\Http\Response
     */
    public function save($id)
    {
        $well = Well::find($id);

        if($well && $well->update(["is_saved" => $well->is_saved ? 0 : 1 ])){
            return response()->json([
                "success" => true
            ]);
        }

        return response()->json([
            "success" => false
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Well  $well
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $well = Well::find($id);

        if($well && $well->delete()){

            return response()->json([
                "success" => true
            ]);
        }

        return response()->json([
            "success" => false
        ]);
    }
}
