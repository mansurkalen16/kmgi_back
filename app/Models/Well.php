<?php

namespace App\Models;

use App\Models\Refs\Field;
use App\Models\Refs\Horizon;
use App\Models\Refs\WellStatus;
use App\Models\Refs\WellType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Well extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function type(){
        return $this->hasOne(WellType::class, "id", "type_id");
    }

    public function status(){
        return $this->hasOne(WellStatus::class, "id", "status_id");
    }

    public function horizon(){
        return $this->hasOne(Horizon::class, "id", "horizon_id");
    }

    public function field(){
        return $this->hasOne(Field::class, "id", "field_id");
    }
}
