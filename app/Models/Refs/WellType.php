<?php

namespace App\Models\Refs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WellType extends Model
{
    use HasFactory;

    protected $guarded = [];

}
