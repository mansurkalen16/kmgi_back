<?php

namespace App\Models\Refs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WellStatus extends Model
{
    use HasFactory;

    protected $guarded = [];

}
