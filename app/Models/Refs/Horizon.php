<?php

namespace App\Models\Refs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Horizon extends Model
{
    use HasFactory;

    protected $guarded = [];

}
