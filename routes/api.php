<?php

use App\Http\Controllers\RefsController;
use App\Http\Controllers\WellsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("wells", [WellsController::class, "index"]);
Route::get("well/{id}", [WellsController::class, "show"]);
Route::post("well", [WellsController::class, "store"]);
Route::post("well/{id}", [WellsController::class, "update"]);
Route::post("well/{id}/save", [WellsController::class, "save"]);
Route::delete("well/{id}", [WellsController::class, "destroy"]);

//refs
Route::get("fields", [RefsController::class, "fields"]);
Route::get("statuses", [RefsController::class, "statuses"]);
Route::get("types", [RefsController::class, "types"]);
Route::get("horizons", [RefsController::class, "horizons"]);
