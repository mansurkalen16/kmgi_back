<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wells', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("field_id");
            $table->string("well");
            $table->unsignedBigInteger("type_id");
            $table->unsignedBigInteger("status_id");
            $table->unsignedBigInteger("horizon_id");
            $table->string("q_liquid")->nullable();
            $table->string("water_cut")->nullable();
            $table->string("oil_density");
            $table->boolean("is_saved");
            $table->timestamps();

            $table->foreign('field_id')->references('id')->on('fields')->restrictOnDelete();
            $table->foreign('type_id')->references('id')->on('well_types')->restrictOnDelete();
            $table->foreign('status_id')->references('id')->on('well_statuses')->restrictOnDelete();
            $table->foreign('horizon_id')->references('id')->on('horizons')->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wells');
    }
}
