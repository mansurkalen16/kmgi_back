<?php

namespace Database\Seeders;

use App\Models\Refs\WellStatus;
use Illuminate\Database\Seeder;

class WellStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ["name" => "Наблюдательная"],
            ["name" => "В работе"],
            ["name" => "В простое"]
        ];
        WellStatus::insert($items);
    }
}
