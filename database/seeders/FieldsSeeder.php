<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Refs\Field;

class FieldsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ["name" => "АА"],
            ["name" => "СС"],
            ["name" => "BB"]
        ];
        Field::insert($items);
    }
}
