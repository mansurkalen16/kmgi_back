<?php

namespace Database\Seeders;

use App\Models\Refs\WellType;
use Illuminate\Database\Seeder;

class WellTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ["name" => "добывающая"]
        ];
        WellType::insert($items);
    }
}
