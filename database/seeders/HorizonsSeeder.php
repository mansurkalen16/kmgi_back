<?php

namespace Database\Seeders;

use App\Models\Refs\Horizon;
use Illuminate\Database\Seeder;

class HorizonsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ["name" => "I"],
            ["name" => "II"],
            ["name" => "III"],
            ["name" => "IV"],
            ["name" => "V"]
        ];
        Horizon::insert($items);
    }
}
